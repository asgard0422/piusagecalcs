# Created by Yifan Jiang 2021-08-31
# For querying data from NetworkHistoryDatabase.db
# and write to selected number of pi tags
# Updated by Yifan Jiang 2021-09-10
# Added capability to query data from powershell cmdlet

import sqlite3
import os
import sys
import clr
import yaml
import logging
from logging import handlers
import re
import subprocess
import traceback

pihome = os.environ['PIHOME']
dbLoc = os.environ['PISERVER']

sys.path.append(pihome + r"\AF\PublicAssemblies\4.0")
clr.AddReference('OSIsoft.AFSDK')

import OSIsoft.AF.Time as OSI_AF_Time  # noqa: E402
import OSIsoft.AF.Data as OSI_AF_Data  # noqa: E402
import OSIsoft.AF.PI as OSI_AF_PI  # noqa: E402
import OSIsoft.AF.Asset as OSI_AF_Asset  # noqa: E402

logger = logging.getLogger('piUsageLog')


def main():
    define_logger(logger)
    try:
        # load configuration file
        config = yaml.safe_load(open(os.path.join(os.getcwd(), r"PIUsageStat_Config.yml")))
    except:  # noqa: E722
        logger.critical("Config File not found")
    try:
        # start check of configuration
        config_check(config)
        # start check for usage counts for the defined time period
        pi_usage_calcs(config['exeName'], config['histTime'], config['piPtName'])
        logger.info("success")
    except:  # noqa: E722
        logger.critical("Application execution failed - refer to previous logs or contact developer")


def create_connection(db_file):
    # database connection creation
    con = None
    try:
        con = sqlite3.connect(db_file)
    except:  # noqa: E722
        logger.error(r'NetworkHistoryDatabase not found at %PISERVER%\dat')
    return con


def select_history_obj(con, freq, exetype):
    # execute sql query to fetch data required
    cur = con.cursor()
    queryStr = 'select count(ch.connection_id), min(ch.start_time), max(ch.end_time), ch.executable_name, cr.user_credentials\
        from connectionHistory as ch left join credentials as cr on ch.connection_id = cr.connection_id\
        where ((select strftime("%s","now","-' + str(freq) + ' ")<<32)<end_time) AND \
        cr.user_credentials not like "%svc-%" AND ch.executable_name like "%' + exetype + \
        '%" \
        group by cr.user_credentials \
        order by start_time DESC'
    cur.execute(queryStr)
    histConStats = cur.fetchall()
    numHistCons = len(histConStats)
    return numHistCons


def select_active_obj(con, exetype):
    cur = con.cursor()
    queryStr = 'select ac.connection_id, ac.start_time, ac.executable_name, cr.user_credentials\
                from activeConnections as ac left join credentials as cr on ac.connection_id = cr.connection_id\
                where cr.user_credentials not like "%svc-%" AND ac.executable_name like "%' + exetype + '%" \
                order by start_time DESC'
    cur.execute(queryStr)
    activeConStats = cur.fetchall()
    numActiveCons = len(activeConStats)
    return numActiveCons


def pi_connect():
    piServer = None
    try:
        piServers = OSI_AF_PI.PIServers()
        piServer = piServers.DefaultPIServer
    except:  # noqa: E722
        logger.error('Unable to connect to default PI server')
    return piServer


def pi_point_update(piServer, ptName, ptValue):
    try:
        writept = OSI_AF_PI.PIPoint.FindPIPoint(piServer, ptName)
    except:  # noqa: E722
        logger.error(f'The pi point {ptName} is not found')
    val = OSI_AF_Asset.AFValue()
    val.Value = ptValue
    val.Timestamp = OSI_AF_Time.AFTime("*")
    try:
        writept.UpdateValue(val, OSI_AF_Data.AFUpdateOption.Insert, OSI_AF_Data.AFBufferOption.BufferIfPossible)
    except:  # noqa: E722
        logger.error(f'The pi point {ptName} update is failed')


def pi_point_read(piServer, ptName):
    # not currently called in the main program
    pt = OSI_AF_PI.PIPoint.FindPIPoint(piServer, ptName)
    name = pt.Name.lower()  # noqa: F841
    # CurrentValue
    # print('\nShowing PI Tag CurrentValue from {0}'.format(name))
    current_value = pt.CurrentValue()  # noqa: F841
    # print('{0}\'s Current Value: {1}'.format(name, current_value.Value))


def pi_usage_calcs(appList, histRange, piPtList):
    dbFileLoc = os.path.join(dbLoc, "dat", r"NetworkHistoryDatabase.db")
    con = create_connection(dbFileLoc)
    piServer = pi_connect()

    if con:
        for appID, app in enumerate(appList):
            if "w3wp" in app.lower():
                psConVar = powershell_cmdexe("$con = Connect-PIDataArchive -PIDataArchiveMachineName localhost; \
                                        Get-PIConnectionStatistics -Connection $con")
                if psConVar.returncode != 0:
                    logger.error(psConVar.stderr)
                else:
                    appDict = constat_classifier(psConVar)
                    ttlCnt = ps_usage_calcs(appDict, app)
            else:
                histCnt = select_history_obj(con, histRange, app)
                actvCnt = select_active_obj(con, app)
                ttlCnt = histCnt + actvCnt
            pi_point_update(piServer, piPtList[appID], ttlCnt)
        con.close()


def config_check(config):
    # check size limits
    exeSize = len(config['exeName'])
    piPtSize = len(config['piPtName'])
    if exeSize != piPtSize:
        logger.error("Config File: Number of elements in exeName section must equal that of piPtName section")
    elif exeSize == piPtSize and exeSize + piPtSize > 0:
        logger.info("Config File: Element size checked successfully")
    else:
        logger.critical("Config File: Unknown error, contact developper")

    # check hist time format
    if len(config['histTime']) > 0:
        histTimeValue = config['histTime'][0].split()[0]
        histTimeUnit = config['histTime'][0].split()[1]
        validUnits = ['minutes', 'days', 'seconds']
        if histTimeValue.isdecimal() and int(histTimeValue) > 0 and histTimeUnit in validUnits:
            logger.info("Config File: Historical time range checked successfully")
        else:
            logger.error("Config File: Historical time range not in correct format ^\d+ w+")  # noqa: W605


def define_logger(logger):
    log_format = "%(asctime)s - %(levelname)s - %(message)s"
    log_level = 10
    handler = handlers.TimedRotatingFileHandler("piUsageLogs.log", when='midnight',
                                                interval=1, backupCount=2)
    formatter = logging.Formatter(log_format)
    handler.setFormatter(formatter)
    handler.suffix = "%Y%m%d"
    handler.extMatch = re.compile(r"^\d{8}$")  # noqa: W605
    logger.setLevel(log_level)
    logger.addHandler(handler)


def powershell_cmdexe(cmd):
    try:
        logger.info(cmd)
        result = subprocess.run(["powershell", "-Command", cmd], capture_output=True, stdin=subprocess.PIPE)

        # p = subprocess.Popen(["powershell.exe", "-Command",cmd], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        # result2 = p.stdout.read()
        # p.communicate()
    except:  # noqa: E722
        logger.error("Powershell execution failed")
        logger.error(traceback.format_exc())
        logger.error(result.stderr)
    return result


def constat_classifier(conStatByte):
    conStatOri = conStatByte.stdout.decode("utf-8")
    conStatLst = conStatOri.split('\r\n')
    appConStat = []
    allAppConStat = {}
    appInfoStrt = False
    try:
        for conStat in conStatLst:
            if conStat.startswith("ID") and conStat.endswith("ID"):
                appInfoStrt = True
                appID = list(filter(None, conStat.split(" ")))[1]
                appConStat.append(conStat)
            elif conStat.startswith("Name"):
                appName = list(filter(None, conStat.split(" ")))[1]
                appConStat.append(conStat)
                logger.info("Name recorded" + str(appName))
            elif conStat.startswith("Trust") and conStat.endswith("Trust"):
                appInfoStrt = False
                appConStat.append(conStat)
                allAppConStat[appID + "_" + appName] = appConStat
                appConStat = []
            elif appInfoStrt:
                appConStat.append(conStat)
    except:  # noqa: E722
        logger.info("No usage information avaialble in the system")
    return allAppConStat


def ps_usage_calcs(appDict, appName):
    allUserSet = set()
    for (key, value) in appDict.items():
        if appName in key:
            for info in value:
                if info.startswith("OSUser"):
                    logger.info("Osuser info is " + info)
                    userInfo = list(filter(None, info.split(" ")))
                    userNames = list(filter(lambda x: False if "OSUser" in x else True, userInfo))
                    userLst = list(filter(lambda x: False if "|" in x else True, userNames))
                    userLst_rmbraces = list(filter(lambda x: False if "{" == x or "}" == x else True, userLst))
                    if len(userLst_rmbraces) > 0:
                        for user in userLst_rmbraces:
                            if "{" in user:
                                user = user.replace("{", "")
                            if "}" in user:
                                user = user.replace("}", "")
                            if "," in user:
                                user = user.replace(",", "")
                            if not ("svc" in user):
                                allUserSet.add(user)
    userCnt = len(allUserSet)
    return userCnt


if __name__ == '__main__':
    main()

# app icon designed by Aficons
# Link: https://iconscout.com/icon-pack/smart-farm-29

# add PISERVER directory - done
# Logging module - log failed write - fail to connect - failed to find the db file
# separate config file for parameters - done
# making sure the len of the exe list and pipnt list are equal - hmi on change of config
